# The Cat Collective's Dotfiles

## Project License
This project is licensed under the BSD 3-Clause License. Please refer to the [LICENSE](LICENSE) file for the full text of the project's license.

## Third-Party Components
1. Bluetooth Scripts
  - Author: Lena Różalski ("Decaying Eagle")
  - License: BSD 3-Clause License
  - Script Paths:
    - `/polybar/scripts/bluetooth.sh`
    - `/polybar/scripts/toggle_bluetooth.sh`

2. polybar-spotify Scripts 
  - Author: Prayag Savsani
  - Project GitHub: [PrayagS/polybar-spotify](https://github.com/PrayagS/polybar-spotify/)
  - License: MIT License
  - Original Project's License: [LICENSE.md](https://github.com/PrayagS/polybar-spotify/blob/master/LICENSE.md)
  - Script Paths:
    - `/polybar/scripts/scroll_status.sh`
    - `/polybar/scripts/get_spotify_status.sh`

## Software Configurations
1. Alacritty
  - Authors: Christian Duerr and Kirill Chibisov
  - Project GitHub: [alacritty/alacritty](https://github.com/alacritty/alacritty)
  - License: Apache License 2.0
  - Original Project's License: [LICENSE-APACHE](https://github.com/alacritty/alacritty/blob/master/LICENSE-APACHE)

2. bspwm
  - Author: baskerville
  - Project GitHub: [baskerville/bspwm](https://github.com/baskerville/bspwm)
  - License: BSD 2-Clause License
  - Original Project's License: [LICENSE](https://github.com/baskerville/bspwm/blob/master/LICENSE)

3. Dunst
  - Authors: Benedikt Heine, Friso Smit, Sascha Kruse, Seeryn, and Nikos Tsipinakis
  - Project GitHub: [dunst-project/dunst](https://github.com/dunst-project/dunst)
  - License: BSD 3-Clause License
  - Original Project's License: [LICENSE](https://github.com/dunst-project/dunst/blob/master/LICENSE)

4. Paru
  - Author: Morganamilo
  - Project GitHub: [Morganamilo/paru](https://github.com/Morganamilo/paru)
  - License: GPL-3.0-only
  - Original Project's License: [LICENSE](https://github.com/Morganamilo/paru/blob/master/LICENSE)

5. Picom
  - Author: Yshui
  - Project GitHub: [yshui/picom](https://github.com/yshui/picom)
  - License: MIT License
  - Original Project's License: [LICENSE](https://github.com/yshui/picom/blob/next/LICENSE.spdx)

6. Polybar
  - Authors: Michael Carlberg, Jérôme Boulmier, NBonaparte, Patrick Ziegler, and x70b1
  - Project GitHub: [polybar/polybar](https://github.com/polybar/polybar)
  - License: MIT License
  - Original Project's License: [LICENSE](https://github.com/polybar/polybar/blob/master/LICENSE)

7. Rofi
  - Author: Davatorium
  - Project GitHub: [davatorium/rofi](https://github.com/davatorium/rofi)
  - License: MIT License
  - Original Project's License: [COPYING](https://github.com/davatorium/rofi/blob/next/COPYING)

8. sxhkd
  - Author: baskerville
  - Project GitHub: [baskerville/sxhkd](https://github.com/baskerville/sxhkd)
  - License: MIT License
  - Original Project's License: [LICENSE](https://github.com/baskerville/sxhkd/blob/master/LICENSE)

9. Starship
  - Authors: Andrew Dassonville, Thomas O'Donnell, Tilmann Meyer, Shu Kutsuzawa, David Knaack, Zhenhui Xie, Jimmy Royer, John Letey, Matan Kushner, Mick Hohmann, Snuggle, Tiffany Le-Nguyen, Dario Vladović, and Niel Kistner
  - Project GitHub: [starship/starship](https://github.com/starship/starship)
  - License: ISC License
  - Original Project's License: [LICENSE](https://github.com/starship/starship/blob/master/LICENSE)
