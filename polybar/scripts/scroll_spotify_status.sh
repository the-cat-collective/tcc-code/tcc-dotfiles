#!/bin/bash
#
# Spotify Scroll Script
#
# SPDX-FileCopyrightText: 2022 Prayag Savsani
# SPDX-License-Identifier: MIT
#
# Please check the LICENSING file in the root directory of this repository for more information.
#
# This script was minimally altered from the original by The Cat Collective to add this SPDX header.

# see man zscroll for documentation of the following parameters
zscroll -l 80 \
        --delay 0.1 \
        --scroll-padding "  " \
        --match-command "`dirname $0`/get_spotify_status.sh --status" \
        --match-text "Playing" "--scroll 1" \
        --match-text "Paused" "--scroll 0" \
        --update-check true "`dirname $0`/get_spotify_status.sh" &

wait

