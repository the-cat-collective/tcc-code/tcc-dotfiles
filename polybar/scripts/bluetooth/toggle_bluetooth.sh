#!/bin/sh
#
# Script to toggle bluetooth
#
# SPDX-FileCopyrightText: 2024 Lena Różalski
# SPDX-License-Identifier: BSD 3-Clause
#
# This script is was incorporated into this repository under the marked license
# with the express written permission of Lena Różalski.

if [ $(bluetoothctl show | grep "Powered: yes" | wc -c) -eq 0 ]; then
        bluetoothctl power on
    else
        bluetoothctl power off
fi
