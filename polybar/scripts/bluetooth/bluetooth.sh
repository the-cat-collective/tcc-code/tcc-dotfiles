#!/bin/sh
#
# Bluetooth Indicator
#
# SPDX-FileCopyrightText: 2024 Lena Różalski
# SPDX-License-Identifier: BSD 3-Clause
#
# This script is was incorporated into this repository under the marked license
# with the express written permission of Lena Różalski.

if [ $(bluetoothctl show | grep "Powered: yes" | wc -c) -eq 0 ]; then
    echo "%{F#66ffffff}"
else
    if [ $(echo info | bluetoothctl | grep 'Device' | wc -c) -eq 0 ]; then 
        echo ""
    fi
    echo "%{F#2193ff}"
fi

