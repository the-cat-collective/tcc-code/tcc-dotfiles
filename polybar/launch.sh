#!/bin/bash
#
# Polybar launch.sh
#
# SPDX-FileCopyrightText: 2024 Michael Carlberg
# SPDX-License-Identifier: MIT
#
# Please check the LICENSING file in the root directory of this repository for more information.
#
# This script has not been altered from the original except to add this SPDX header.

# Terminate already running bar instances
killall -q polybar
# If all your bars have ipc enabled, you can also use
# polybar-msg cmd quit

# Launch Polybar, using default config location ~/.config/polybar/config.ini
polybar mybar 2>&1 | tee -a /tmp/polybar.log & disown
polybar myotherbar 2>&1 | tee -a /tmp/polybar2.log & disown
echo "Polybar launched..."
