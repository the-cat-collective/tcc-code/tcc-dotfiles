# The Cat Collective's Dotfiles

## Project Information
This repository hosts all of The Cat Collective's configuration files (dotfiles) for the X11 Window Manager (WM) BSPWM.

## Caveats
This project is best suited for being run on Arch Linux and related distributions (such as Endeavour OS, Manjaro Linux, Arco Linux, Crystal Linux, etc...) due to the ease of acquiring some of the more esoteric dependencies via the AUR. Installing these dotfiles on other Linux distributions (such as Fedora and Debian) may eventually be documented here.

This guide also assumes that the user is familiar with configuring the software listed here, and so, will not contain a ricing guide or such to explain how to set these files beyond moving them around.

## Autoinstall script
Please note that the autoinstall script has been moved to [its own repo](https://gitlab.com/the-cat-collective/tcc-code/tcc-dotfiles-autoinstaller). Thank you for understanding.

## Getting Started
This guide assumes that you wish to clone this repo and replace your current configuration with it completely. If you want to pick and choose your components, go ahead, but we can't help you if something breaks. You are on your own.

First, install base-devel` and `git`
```bash
# sudo pacman -S base-devel git
```

Then download the dependencies for the project and then clone the repo.
This project requires:
```bash
alacritty
blueberry
bspwm
dunst
ttf-fira-code
ttf-fira-code-nerd
flameshot
git
gnome-themes-extra
lxappearance
mate-polkit # (any polkit helper will do)
noto-fonts
noto-fonts-cjk
paru # (any AUR helper will do)
picom
playerctl
polybar
rofi
slock
sxhkd
xidlehook # (AUR)
xscreensaver
zscroll-git # (AUR)
```
** Anything listed as "(AUR)" must be compiled from source by being downloaded from the AUR.

Required build dependencies:
`rust` for `paru` and `xidlehook`
`python` for `zscroll-git`

## Cloning the repo
Run the following command to clone the sources:
```bash
git clone https://gitlab.com/the-cat-collective/tcc-code/tcc-dotfiles.git
```

## Installation
Now for the fun part:
Back up your existing `$HOME/.config` if it exists and then copy over all the files here:
```bash
$ cp -r ~/.config ~/.config.bak
$ rm -rf ~/.config
$ mkdir ~/.config
$ cd Downloads
$ mv tcc_dotfiles/* ~/.config/
```

## Project Structure
The root of the project contains subdirectories which individually contain configs for each piece of software. The root of the project also contains the `README` and `LICENSE` files.

## Licensing
* This project is licensed under the BSD 3-Clause License. Please refer to the [LICENSE](LICENSE) file for the full text of the project's license.

* For licensing information related to third-party components used in this project, please check the [LICENSING](LICENSING) file.

## Disclaimer
None